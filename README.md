<div align="center">
  <h2>Scope</h2>
  <p>Los datos se convirtieron en la nueva materia prima de los negocios. <b>Privacidad y Protección de Datos. #DataPrivacity</b></p>
</div>

<div align="center">
  <a href="https://dataprivacity.com/" target="_blank">
    <img src="assets/images/logo.png" width="200">
  </a>
  <h1>@DataPrivacity</h1>
</div>

<div align="center">
  <p>Aprende en nuestras redes:</p>
  <a href="https://DataPrivacity.com/" target="_blank">
    <img src="assets/images/globe.svg" width="50">
  </a>
  <a href="https://twitter.com/DataPrivacity" target="_blank">
    <img src="assets/images/twitter.svg" width="50">
  </a>
  <a href="https://instagram.com/DataPrivacity" target="_blank">
    <img src="assets/images/instagram.svg" width="50">
  </a>
  <a href="https://www.facebook.com/DataPrivacity/" target="_blank">
    <img src="assets/images/facebook.svg" width="50">
  </a>
  <a href="https://youtube.com/" target="_blank">
    <img src="assets/images/youtube.svg" width="50">
  </a>
  <a href="https://linkedin.com/company/DataPrivacity" target="_blank">
    <img src="assets/images/linkedin.svg" width="50">
  </a>
</div>

<div align="center">
  <p>Alojamos proyectos en:</p>
  <a href="https://gitlab.com/DataPrivacity" target="_blank">
    <img src="assets/images/gitlab.svg" width="50">
  </a>
  <a href="https://github.com/DataPrivacity" target="_blank">
    <img src="assets/images/github.svg" width="50">
  </a>
    <a href="https://drive.google.com/drive/folders/1uHOoUbx83PSKySfbxBOqwlthUA6ofHYP?usp=sharing" target="_blank">
      <img src="assets/images/download.svg" width="50">
  </a>
</div>

<div align="center">
  <p>DR, Cofundador de: DataPrivacity</p>
  <a href="https://twitter.com/wiiiccho" target="_blank">
    <img src="assets/images/cofundador.png" width="50">
  </a>
</div>

Todo el contenido publicado es modificable, si tu quieres colaborar, ves errores o añadir contenido, puedes hacerlo; cumpliendo los siguientes requisitos:

<div align="center">
  <b>“Todos para uno, uno para todos.”</b>
</div>
<br>
<div align="center">
  <b>“Llegar juntos es el principio; mantenerse juntos es el progreso; trabajar juntos es el éxito.”</b>
</div>
<br>

<div align="center">
  <b>Mi trabajo en el software libre está motivado por un objetivo idealista: difundir libertad y cooperación. Quiero motivar la expansión del software libre, reemplazando el software privativo que prohíbe la cooperación, y de este modo hacer nuestra sociedad mejor. Richard Stallman</b>
</div>

# Tabla de contenido
- [Hoisting](#Hoisting)
    - [¿Qué es el hoisting?](#¿Qué-es-el-hoisting?)
- [Scope](#Scope)
    - [¿Qué es el Scope?](#¿Qué-es-el-Scope?)
    - [Global Scope](#Global-Scope)
    - [Local Scope](#Local-Scope)
    - [Global automático](#Global-automático)
    - [Lexical Scope](#Lexical-Scope)
    - [Block Scope](#Block-Scope)
    - [Function Scope](#Function-Scope)
- [Closure](#Closure)
    - [¿Qué es un closure?](#¿Qué-es-un-closure?)
    - [Variables privadas con closures](#Variables-privadas-con-closures)
    - [Debugging](#Debugging)
- [Extensión](#Extensión)
    - [Extensiónes recomendados](#Extensiónes-recomendados)

# <a name="Hoisting">Hoisting</a>
## <a name="¿Qué-es-el-hoisting?">¿Qué es el hoisting?</a>

Hoisting es un término para describir que las declaraciones de variables declaradas con var y funciones son desplazadas a la parte superior del scope más cercano. Hosting, o elevación es el comportamiento por defecto de JavaScript de “mover declaraciones al principio” del código. Hosting es un término que se puede encontrar a partir de ECMAScript® 2015 Language Specification.

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// Hoisting en variables declaradas con var
-- -------------------------------------------------------------------------------------------- --

// Declarada
var a;
// Inicializada
a = "Data privacity";

-- -------------------------------------------------------------------------------------------- --

console.log(x); // output => undefined
var x = 10;
console.log(x); // output => 10

// La variable se eleva y pasa a declararse al comienzo de su contexto

var x;          // Se elevo la declaración
console.log(x); // output => undefined
x = 10;
console.log(x); // output => 10

-- -------------------------------------------------------------------------------------------- --

var x = 1;                // variable declarada y inicializada
console.log(x + " " + y); // output => 1 undefined
var y = 2;                // variable inicializada

// JavaScript sólo utiliza el hoisting en declaraciones. Como se puede apreciar la elevación 
// afecta la declaración de variables, pero no su inicialización.

var x = 1;                // variable declarada y inicializada
var y;                    // Se elevo la declaración
console.log(x + " " + y); // output => 1 undefined
y = 2;                    // variable inicializada

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
```

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// Hoisting en funciones
-- -------------------------------------------------------------------------------------------- --

function miNombre(nombre) {
  console.log("El nombre es " + nombre);
}
miNombre("DataPrivacity");
// output
// El nombre es DataPrivacity

// Las declaraciones de funciones antes de ejecutar cualquier otro segmento de código. Permite 
// utilizar una función antes de declararla en el código.  Pero, lo que realmente sucede es que 
// JavaScript guarda la función en memoria, no asigna undefined como con las variables. Por 
// ejemplo:

miNombre("DataPrivacity");
function miNombre(nombre) {
  console.log("El nombre es " + nombre);
}
// output
// El nombre es DataPrivacity

-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
```

#### Buenas prácticas 

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Recomendaciones en funciones
// Lo recomendable es declarar asignar las funciónes con sus instrucciones y por ultimo mandar 
// a llamar la funcion

function miFuncion (){
  let nombre = 'Data';
  console.log(`${nombre}`);
}
miFuncion();
// output
// Data

-- -------------------------------------------------------------------------------------------- --

// Recomendaciones en variables 
// Nunca mas usar var y simplemente usar let o const. Primero debemos de declararlas y 
// inicializarlas despues asi utilizarlas

let hola = "Hola, mundo";
console.log(hola);
// output
// Hola, mundo

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

- [Hoisting](https://developer.mozilla.org/es/docs/Glossary/Hoisting)

# <a name="Scope">Scope</a>
## <a name="¿Qué-es-el-Scope?">¿Qué es el Scope?</a>

El contexto actual de ejecución. El contexto en el que los valores y las expresiones son visibles o pueden ser referenciados. Si una variable u otra expresión no está en el **Scope alcance actual**, entonces no está disponible para su uso. Los Scope también se pueden superponer en una jerarquía, de modo que los Scope secundarios tengan acceso a los ámbitos primarios, pero no al revés.

#### En JavaScript tenemos distintos tipos de Scopes:

- Global Scope
- Local Scope
- Global automática
- Lexical Scope
- Block Scope
- Function Scope

## <a name="Global-Scope">Global Scope</a>

En un entorno de programación, el ámbito global es el ámbito que contiene y es visible en todos los demás ámbitos. En JavaScript del lado del cliente, el alcance global es generalmente la página web dentro de la cual se ejecuta todo el código.

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

let scope = "Global Scope";

console.log(scope);
// output
// Global Scope

function miFuncion() {
  console.log(scope);
}

miFuncion();
// output
// Global Scope

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

## <a name="Local-Scope">Local Scope</a>

El alcance local esta definido dentro de un bloque de codigo o en una función. Son variables locales, es decir se encuentran en el Scope local. Esto significa que este tipo de variables van a vivir únicamente dentro de la función en donde las hayamos declarado y si intentamos accederlas fuera de ella, dichas variables no van a estar definidas.

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
// Ejemplo con un bloque de codigo
-- -------------------------------------------------------------------------------------------- --

{
  let scope = "Local Scope";
  console.log(scope);
  // output
  // Local Scope
}

console.log(scope);
// output
// scope is not defined

-- -------------------------------------------------------------------------------------------- --
// Ejemplo con una función
-- -------------------------------------------------------------------------------------------- --

function myFunction() {
  let scope = "Local Scope";
  console.log(scope);
}
myFunction();
// output
// Local Scope

console.log(scope);
// output
// scope is not defined

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

## <a name="Global-automático">Global automático</a>

Si se asigna una  variable dentro de un bloque de codigo o una una función sin las palabras reservadas let y const será una variable global. Es por ello que es mala práctica crear una variable sin las palabras reservadas.

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --
/* 
Ejemplo con un bloque de codigo
Asignamos un valor a una variable que no ha sido declarada, esta se convertirá 
automáticamente en una variable global.
*/
-- -------------------------------------------------------------------------------------------- --

{
  variable = "variable global";
}

console.log(variable);
// output
// variable global

-- -------------------------------------------------------------------------------------------- --
/* 
Ejemplo con una función
Asignamos un valor a una variable que no ha sido declarada, esta se convertirá 
automáticamente en una variable global.
*/
-- -------------------------------------------------------------------------------------------- --

function miFuncion() {
  variable = "variable global";
  console.log(variable);
}
miFuncion();
// output
// variable global

console.log(variable);
// output
// variable global

-- -------------------------------------------------------------------------------------------- --
/* 
Ejemplo con una función
Asignamos un valor a una variable que no ha sido declarada, esta se convertirá 
automáticamente en una variable global.
*/
-- -------------------------------------------------------------------------------------------- --

function miFuncion(){
  return variable = "variable global";
}

console.log(miFuncion());
// output
// variable global

console.log(variable);
// output
// variable global

-- -------------------------------------------------------------------------------------------- --
/*
Ejemplo de doble asignación de una variable
*/
-- -------------------------------------------------------------------------------------------- --

function miFuncion(){
  let variable = variableUno = "variable global";
  console.log(variable);
  console.log(variableUno);
}
miFuncion()
// output
// variable global
// variable global

console.log(variable);
// output
// variable is not defined

console.log(variableUno);
// output
// variable global

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

## <a name="Lexical-Scope">Lexical Scope</a>

El lexical scope significa que en un grupo anidado de funciones, las funciones internas tienen acceso a las variables y otros recursos de su ámbito padre. Esto significa que las funciones hijas están vinculadas léxicamente al contexto de ejecución de sus padres.

JavaScript usa una cadena de alcance para encontrar variables accesibles en un determinado alcance. Cuando se hace referencia a una variable, JavaScript la buscará en el ámbito actual y continuará con los ámbitos principales hasta que alcance el ámbito global. Esta cadena de alcances atravesados ​​se denomina cadena de alcance.

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

function myFunction() {

  let nombre = "Data Provacity";
  // La variable nombre si es accesible desde acá.
  console.log(nombre);
  // No podemos acceder a la variable edad desde acá.

  function myFunctionUno() {

    // La variable nombre si es accesible desde acá.
    console.log(nombre);
    // No podemos acceder a la variable edad desde acá.

    function myFunctionDos() {  

      // La variable nombre si es accesible desde acá.
      console.log(nombre);
      let edad = 78;

    };
    myFunctionDos();
  };
  myFunctionUno();
};
myFunction();

-- -------------------------------------------------------------------------------------------- --

let scope = "Soy una variable global";

function miFuncion(){

  console.log(scope);
  
  function miFuncionUno(){

    console.log(scope);

  }
  miFuncionUno()
}
miFuncion();

-- -------------------------------------------------------------------------------------------- --

function myFunction() {

  // No podemos acceder a la variable nombre desde acá
  console.log(nombre);

  function myFunctionUno() {
  
    // La variable nombre no es accesible aca
    console.log(nombre);

    function myFunctionDos() {      
      
      let nombre = "Data Provacity";
      // La variable nombre si es accesible desde acá.
      console.log(nombre);

    };
    myFunctionDos();
  };
  myFunctionUno();
};
myFunction();

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

## <a name="Block-Scope">Block Scope</a>

A diferencia del scope local este scope está limitado al bloque de código donde fue definida la variable. Desde ECMAScript 6 contamos con los keyword let y const los cuales nos permiten tener un scope de bloque, esto quiere decir que las variables solo van a vivir dentro del bloque de código correspondiente.

Una declaración de bloque (o declaración compuesta en otros idiomas) se usa para agrupar cero o más declaraciones. El bloque está delimitado por un par de llaves(corchetes).

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

{
  var scope = "Block Scope con var";

  let scopeUno = "Block Scope con let";

  const scopeDos = "Block Scope con const";
}

console.log(scope);
// output
// Block Scope

console.log(scopeUno); 
// output
// scopeUno is not defined

console.log(scopeDos); 
// output
// scopeDos is not defined

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

#### Ejemplos

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Ejemplo de Block Scope, llave abierta y una cerrada {}

// Instrucciones/Declaraciones escritos en el Global Scope podran acceder a la 
// variable scope.
var scope = "Global Scope";
{
  let scopeUno = "Block Scope";
  // Instrucciones/Declaraciones escritos en el Block Scope podran acceder a la 
  // variable scopeUno.
  console.log(scopeUno);
}
console.log(scope);

-- -------------------------------------------------------------------------------------------- --

// Mala practica usar var se mostraba el numero diez, diez veces en consola, 
// tiene que ver con el Hoisting y Asincronía o EventLoop.

function myFuncion() {
    for(var i = 0; i < 10; i++) {
		setTimeout(function() {
			console.log(i);
		}, 1000);
	}
	console.log(i);
}
myFuncion();

// El Hoisting hace que la variable con el nombre i dentro del for se “eleve” y 
// sea asignada en memoria, por lo tanto a lo último se estará reasignando el 
// ultimo incremento que tuvo y se podrá acceder a ella en cualquier punto dentro 
// de la función myFuncion.

function myFuncion() {
	var i;
	for(i = 0; i < 10; i++) {
		setTimeout(function() {
			console.log(i);
		}, 1000);
	}
    // Este console.log podra acceder a i aun afuera del for, esto por que la variable 
    // i fue declarada y asignada antes que todo en tiempo de ejecución (Hoisting).
	console.log(i);
}
myFuncion();
// El valor final de i es de 10
// 10
// 10
// 10
// 10
// 10
// 10
// 10
// 10
// 10
// 10

// ¿Por que el console.log que esta adentro del setTimout solo imprime el valor 
// final de i?

// 1
// El for estará ejecutándose en el Call Stack mientras la condición propuesta en el 
// for (i < 10) se cumpla, por tanto también se ejecutara el setTimeout cada que la 
// condición del for sea verdadera, sin embargo hay que recordar que el setTimeout 
// es una función del navegador, entonces el setTimeout pasara a ejecutarse en 
// “segundo plano” y una vez que los tiempos de espera definidos de los setTimeout 
// se cumplan, estos pasaran al Callback Queue.

// 2
// Una vez que la condición del for ya no se cumpla, pasara a ejecutar el console.log 
// que mostrará el valor final que para entonces será igual a 10 por que el for ya 
// estuvo mutando el valor con cada incremento (i++).

// 3
// Posteriormente ya cuando el Call Stack esta vació pasaran a ejecutarse el callback de 
// cada setTimeout que a su vez ejecutan un console.log imprimiendo el valor de la variable 
// i, recordando que i ya tiene el valor final de 10 por que se había incrementado en uno 
// en cada “vuelta” que daba el for.

// Buena practica es siempre usar let

// Con let, como saben, no se puede sobreescribir la variable, y tenemos un “let” 
// diferente por cada iteración (Imaginen que cada iteración es un bloque de código 
// totalmente diferente), entonces, cada setTimeout agarra el “let” que está dentro 
// de su propio bloque de código, y como es único gracias al scope, ahí si que se 
// imprimen del 0 al 9.
function miFuncion(){
  for (let i = 0; i < 10; i++) {
    setTimeout(() => {
      console.log(i);
    }, 1000)
  }
}
miFuncion();

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

- [loupe](http://latentflip.com/loupe/?code=CgoKZm9yKGxldCBpPTE7aTw1O2krKyl7CiAgICAKc2V0VGltZW91dChmdW5jdGlvbiAoKXsKY29uc29sZS5sb2coaSkKfSwyMDAwKTsKCn0KCg%3D%3D!!!)

## <a name="Function-Scope">Function Scope</a>

Cada función crea un nuevo alcance. El alcance determina la accesibilidad (visibilidad) de estas variables. Las variables definidas dentro de una función no son accesibles (visibles) desde fuera de la función.

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

function miFuncio(){
  let scope = "Function Scope";
  console.log(scope);
};
miFuncio();
// output
// Function Scope

console.log(scope);
// output
// scope is not defined at

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

# <a name="Closure">Closure</a>
## <a name="¿Qué-es-un-closure?">¿Qué es un closure?</a>

Una clausura o closure es una función que guarda referencias del estado adyacente (ámbito léxico). En otras palabras, una clausura permite acceder al ámbito de una función exterior desde una función interior. En JavaScript, las clausuras se crean cada vez que una función es creada.

#### Lexical Scope

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// La función iniciar es una función externa.
function iniciar() {
  
  // La variable nombre es una variable local creada por iniciar.
  let nombre = "Hola, mundo";
  
  // La función mostrarNombre es una función interna, una clausura.
  function mostrarNombre() {
  
  // nombre es una variable declarada en la función externa.
  console.log(nombre);
  
  }
  mostrarNombre();
}
iniciar();
// output
// Hola, mundo

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

#### Closure

Si se ejecuta este código tendrá exactamente el mismo efecto que el ejemplo anterior: se mostrará el texto "Hola, mundo" en un cuadro de alerta de Javascript. Lo que lo hace diferente (e interesante) es que la función externa nos ha devuelto la función interna muestraNombre() antes de ejecutarla.

Puede parecer poco intuitivo que este código funcione. Normalmente, las variables locales dentro de una función sólo existen mientras dura la ejecución de dicha función. Una vez que creaFunc() haya terminado de ejecutarse, es razonable suponer que no se pueda ya acceder a la variable nombre. Dado que el código funciona como se esperaba, esto obviamente no es el caso.

La solución a este rompecabezas es que miFunc se ha convertido en un closure. Un closure es un tipo especial de objeto que combina dos cosas: una función, y el entorno en que se creó esa función. El entorno está formado por las variables locales que estaban dentro del alcance en el momento que se creó el closure. En este caso, miFunc es un closure que incorpora tanto la función muestraNombre como el string "Hola, mundo" que existían cuando se creó el closure.

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

function creaFunc() {

  let nombre = "Hola, mundo";
  
  function muestraNombre() {
  
    alert(nombre);
  
  }
  return muestraNombre;
}

let miFunc = creaFunc();
miFunc();
// output
// Hola, mundo

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

#### Practicando Closures

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Sin Closure
// Si creamos una alcancía de la siguiente manera, solamente mostrará el valor enviado, no 
// guardará la información del dinero que le enviamos.

function alcancia(monedas){
  let contenido = 0;
  contenido = contenido + monedas;
  return contenido;
}

const miAlcancia = alcancia;
console.log(miAlcancia(5));
console.log(miAlcancia(10));

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Con Closure
// Si utilizamos un closure, entonces la variable en que estará en un scope más elevado que la 
// función interior, recordará el ámbito léxico que fue creada. Por lo tanto, cada vez que 
// invoquemos cada función, mostrará el dinero ahorrado en la alcancía.

function alcancia(cantidadInicial){
  let contenido = cantidadInicial
  return function guardar(monedas){
    contenido = contenido + monedas
    return contenido
  }
}

const alcancia_b = alcancia(10);
console.log(alcancia_b(2));
console.log(alcancia_b(1));

-- -------------------------------------------------------------------------------------------- --

// Con Closure

const alcancia = () => {
  let contenido = 0;
  const countCoins = (monedas) => {
    contenido += monedas;
    console.log(`Mi ahorro: ${contenido}`);
  }
  return countCoins;
};

let miAlcancia = alcancia();
miAlcancia(2);
miAlcancia(2);
miAlcancia(2);

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Con Closure

const moneyCounter = (i) => {
  let contenido = i;
  const counts = () => {
    console.log(`valor ${contenido++}`);
  };
  return counts;
};

const counter = moneyCounter(1);
counter();
counter();
counter();

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Con closure explicando

const papa = (nuevoEngendro) =>{
  let cantidadDeHijos = 0;
  const crearEngendro = (nuevoEngendro) =>{
      cantidadDeHijos += nuevoEngendro;
      console.log(`cantidad de engendritos = ${cantidadDeHijos}`)
  }
  return crearEngendro;
}

let traigamosHijosAlMundo = papa();
// cantidad de engendritos = 1
traigamosHijosAlMundo(1);
// cantidad de engendritos = 2
traigamosHijosAlMundo(1);
// cantidad de engendritos = 3
traigamosHijosAlMundo(1);

// Si estudiamos la función papa esta tiene la declaración de la variable cantidaDeHijos y 
// la declaración de la función crearEngendro y al final retorna la función crearEngendro.
// Ahora después de la declaración de la función papa viene la línea. Notese el PARENTESIS() 
// de la función papa, este paréntesis indica que la función se está ejecutando.

let traigamosHijosAlMundo = papa();

// Así que finalmente traigamosHijosAlMundo vale lo que haya retornado la función papa que es 
// crearEngendro (nótese que no se ejecuta la función crearEngendro sino que simplemente se 
// retorna la referencia a ella) por ende cada vez que ejecutamos.

traigamosHijosAlMundo(1)

// Realmente estamos es llamando a la función crearEngendro con todo su ámbito que es la 
// variable cantidadDeHijos con valor de 0, y a este le suma lo que se le pase por parámetro,
// En la segunda ejecución de

traigamosHijosAlMundo(1)

// No se está volviendo a correr todas las líneas de la función papa, esto ya se hizo en
// la asignación ( let traigamosHijosAlMundo = papa(); ), sino que realmente se está volviendo
// a llamar a crearEngendro() la cual había modificado su variable cantidadDeHijos en la 
// primera llamada

// Asi que pienso que la clave es entender que en esta asignación
let traigamosHijosAlMundo = papa();

// Se ejecutó la función papá donde se declararó la variable de dicha función (cantidadDeHijos)
// y la función que retorna (crearEngendro) UNA SOLA VEZ! lo que se ejecuta multiples veces 
// es el la función crearEngendro.

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

## <a name="Variables-privadas-con-closures">Variables privadas con closures</a>

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

const person = () => {
  let variablePrivada = "DataPrivacity";
  return {
    getName: () => variablePrivada,
    setName: (name) => {
      variablePrivada = name;
    },
  };
};

const newPerson = person();
console.log(newPerson.getName());
newPerson.setName('Organización');
console.log(newPerson.getName());

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

- [Patrones de diseño en JavaScript](https://medium.com/@jmz12/patrones-de-dise%C3%B1o-en-js-43beab8f5756)

## <a name="Debugging">Debugging</a>

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

var a = "Data";
function hello () {
    let b = 'Hello world';
    const c = 'Hello world!!';
    if (true) {
        let d = '¡¡Hello world!!';
        debugger;
    }
}

hello();

-- -------------------------------------------------------------------------------------------- --

const moneyBox = () => {
  debugger
  let contenido = 0;
  const countCoins = (coins) => {
    debugger
    contenido += coins;
    console.log(`MoneyBox: $${contenido}`);
  }
  return countCoins;
};

let myMoneyBox = moneyBox();

myMoneyBox(4);
myMoneyBox(6);
myMoneyBox(10);

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```

# <a name="Extensión">Extensión</a>
## <a name="Extensiónes-recomendados">Extensiónes recomendados</a>

- Code Runner
- nodemon

```js
-- ############################################################################################ --
-- -------------------------------------------------------------------------------------------- --

// Generamos una pagina en blanco en el navegador
about:blank

-- -------------------------------------------------------------------------------------------- --
-- ############################################################################################ --
```